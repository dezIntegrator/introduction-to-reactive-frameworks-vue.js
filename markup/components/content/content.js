$(document).ready(function() {
    Vue.component('article-template', {
    props: ['item', 'sections'], 
    template: 
    `
        <article class="s-content__articles_article">
            <div class="article__preview-container" v-if="item.url === ''">
                <img class="article__preview-container_image" :src="item.image" alt="">
            </div>
            <div class="article__preview-container" v-else>
                <a :href="item.url">
                    <img class="article__preview-container_video" :src="item.image" alt="">
                </a>
            </div>
            <div class="article__content-container">
                <h3 class="article__content-container_section" v-if="item.url === ''">{{ sections[item.sectionId].title }}</h3>
                <h3 class="article__content-container_section" v-else>video {{ sections[item.sectionId].title }}</h3>
                <h3 class="article__content-container_title">{{ item.title }}</h3>      
                <p class="article__content-container_description">{{ item.description }}</p>
                <div class="article__content-container_link-container">
                <a class="article__content-container_link" :href="item.link" v-if="item.readmore">read more</a>
                </div>
            </div>
        </article>
    `
    });

    let articles = new Vue({
    el: '#contentApp',
    delimiters: ['${', '}'],
    data: {
        keywords: '',
        selectedSpeciality: 'All speciality',
        selectedSection: 'All section',
        selectedAudience: 'All audience',
        showDropdownSpeciality: false,
        showDropdownSection: false,
        showDropdownAudience: false,
        items: [
            {
                id: 0,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 1,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 2,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 3,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 4,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 5,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 6,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 7,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 8,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 9,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 10,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 11,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 12,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 13,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 14,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 15,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 16,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 17,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 18,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 19,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 20,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 21,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 22,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 23,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 24,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 25,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 26,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 27,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 28,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 29,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 30,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 31,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 32,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 33,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 34,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 35,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 36,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 37,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 38,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 39,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 40,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 41,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 42,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 43,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 44,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 45,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 46,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 47,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 48,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 49,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 50,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 51,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 52,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 53,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 54,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 55,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 56,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 57,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 58,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 59,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 60,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 61,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 62,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 63,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 64,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 65,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 66,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 67,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 68,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 69,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 70,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 71,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 72,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 73,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 74,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 75,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 76,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 77,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 78,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 79,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 80,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 81,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 82,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 83,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 84,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 85,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 86,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 87,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 88,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 89,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 90,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 91,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 92,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 93,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 94,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 95,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 96,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 97,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 98,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 99,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 100,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 101,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 102,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 103,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 104,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 105,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 106,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 107,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 108,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 109,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 110,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 111,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 112,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 113,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 114,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 115,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 116,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 117,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 118,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 119,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 120,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 121,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 122,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 123,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 124,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 125,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 126,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 127,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 128,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 129,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 130,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 131,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 132,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 133,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 134,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 135,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 136,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 137,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 138,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 139,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 140,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 141,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 142,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 143,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 144,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 145,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 146,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 147,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 148,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 149,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 150,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 151,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 152,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 153,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 154,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 155,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 156,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 157,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 158,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 159,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 160,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 161,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 162,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 163,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 164,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 165,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 166,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 167,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 168,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 169,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 170,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 171,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 172,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 173,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 174,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 175,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 176,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 177,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 178,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 179,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 180,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 181,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 182,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 183,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 184,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 185,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 186,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 187,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 188,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 189,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 190,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 191,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 192,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 193,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 194,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 195,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 196,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 197,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 198,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 199,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 200,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 201,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 202,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 203,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 204,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 205,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 206,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 207,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 208,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 209,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 210,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 211,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 212,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 213,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 214,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 215,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 216,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 217,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 218,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 219,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 220,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 221,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 222,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 223,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 224,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 225,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 226,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 227,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 228,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 229,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 230,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 2,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 231,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 232,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 233,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Spine',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 234,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 235,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 236,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 237,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 238,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 239,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: false
              },
              {
                id: 240,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 2,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 241,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 242,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 3,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 243,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              },
              {
                id: 244,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Scientists',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 245,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 246,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Trauma',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: false
              },
              {
                id: 247,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Ortho',
                sectionId: 1,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Numquam, cum enim? A modi explicabo quis perferendis esse hic optio placeat deserunt numquam cupiditate, tenetur error, veniam tempora ab earum culpa voluptate ipsa, accusamus quod minima. Nisi, quidem? Labore.',
                link: '#',
                readmore: true
              },
              {
                id: 248,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Spine',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: false
              },
              {
                id: 249,
                image: 'static/img/content/1.jpg',
                url: '',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Beatae, tempore animi fugit, ducimus totam quam ratione eaque tempora reprehenderit voluptatum laborum rerum consequuntur! Eos quaerat soluta vero?',
                link: '#',
                readmore: true
              },
              {
                id: 250,
                image: 'static/img/content/2.jpg',
                url: '#',
                speciality: 'Dental',
                sectionId: 3,
                audience: 'Students',
                title: 'Polymeric Materials for Articulating Motion Preservation Devices',
                description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit dolores alias ad quam. Doloribus sapiente iure reprehenderit et',
                link: '#',
                readmore: true
              }
        ],
        sections: [
            { 
                id: 0,
                title: 'All section'
            },
            { 
                id: 1,
                title: 'Case Study',
                showAll: false,
                empty: true
            },
            { 
                id: 2,
                title: 'Product Information',
                showAll: false,
                empty: true
            },
            { 
                id: 3,
                title: 'Abstract summary',
                showAll: false,
                empty: true
            }
        ],
        specialities: [
            {
                title: 'All speciality'
            },
            {
                title: 'Ortho'
            },
            {
                title: 'Spine'
            },
            {
                title: 'Dental'
            },
            {
                title: 'Trauma'
            }
        ],
        audiences: [
            {
                title: 'All audience'
            },
            {
                title: 'Students'
            },
            {
                title: 'Scientists'
            }
        ]
    },
    computed: {
        allItems: function () {
            let keywords = this.keywords;
            let selectedSpeciality = this.selectedSpeciality;
            let selectedSection = this.selectedSection;
            let selectedAudience = this.selectedAudience;
            let itemsInSection = 0;
            let currentSection = '';
            let sortedItems = this.items;
            let sections = this.sections;      
            
            for (let i = 1; i < sections.length; i++) {
              this.sections[i].empty = true;
            }

            sortedItems.sort(function (a, b) {
                if (a.sectionId > b.sectionId) {
                  return 1;
                }
                if (a.sectionId < b.sectionId) {
                    return -1;
                  }
                  return 0;
                });

            return sortedItems.filter(function (item) {
                if(keywords.length>0)
                    {
                        if(item.title.indexOf(keywords) < 0 && item.description.indexOf(keywords) < 0)
                          {
                            return;
                          }
                    }

                if(selectedSpeciality != 'All speciality' && selectedSpeciality != item.speciality)
                    {
                        return;
                    }

                if(selectedSection != 'All section' && selectedSection != sections[item.sectionId].title)
                    {
                        return;
                    }

                if(selectedAudience != 'All audience' && selectedAudience != item.audience)
                    {
                        return;
                    }

                if(sections[item.sectionId].showAll)
                {
                    sections[item.sectionId].empty = false;
                    return item;
                } else {
                    if(currentSection != sections[item.sectionId].title)
                        {
                          sections[item.sectionId].empty = false;
                          console.log(sections[item.sectionId].empty);
                          currentSection = sections[item.sectionId].title;
                          itemsInSection = 0;
                        }
                    itemsInSection++;
                    if(itemsInSection>3)
                    {
                        return;
                    } else {
                        return item;
                    }
                }
            });
        }
    },
    created () { 
        document.addEventListener('click', this.dropdownClose);
    }, 
    destroyed () { 
        document.removeEventListener('click', this.dropdownClose);
    }, 
    methods: {
        expandList: function (n) {
          let btnSection = n;
          let sections = this.sections;              
          sections[btnSection].showAll = true;  
        },
        switchDropdown: function (e) {
            let dropdown = e.target.dataset.vue;
            if (dropdown === 'speciality') {
                this.showDropdownSpeciality = !this.showDropdownSpeciality;
                this.showDropdownSection = false;
                this.showDropdownAudience = false;
            } else {
                if (dropdown === 'section') {
                    this.showDropdownSection = !this.showDropdownSection;
                    this.showDropdownSpeciality = false;
                    this.showDropdownAudience = false;
                } else {
                    if (dropdown === 'audience') {
                        this.showDropdownAudience = !this.showDropdownAudience;
                        this.showDropdownSpeciality = false;
                        this.showDropdownSection = false;
                    }
                }
            }
        },
        selectDropdownItem: function (e) {
            for (let i = 1; i < this.sections.length; i++) {
              this.sections[i].showAll = false;
            }

            let dropdown = e.target.dataset.vue;
            let dropdownItemText = e.target.outerText;

            if (dropdown === 'speciality') {
                this.showDropdownSpeciality = false;
                this.selectedSpeciality = dropdownItemText;
            } else {
                if (dropdown === 'section') {
                    this.showDropdownSection = false;
                    this.selectedSection = dropdownItemText;
                } else {
                    if (dropdown === 'audience') {
                        this.showDropdownAudience = false;
                        this.selectedAudience = dropdownItemText;
                    }
                }
            }
        },
        dropdownClose (e) { 
            let el = this.$refs.dropdownClose;
            let target = e.target;
            if (el !== target && !el.contains(target)) 
            { 
              this.showDropdownSpeciality = false;
              this.showDropdownSection = false;
              this.showDropdownAudience = false;
            } 
        },
        inputChange: function () {
          for (let i = 0; i < this.sections.length; i++) {
            this.sections[i].showAll = false;
          }
        }
      }
    });
});
